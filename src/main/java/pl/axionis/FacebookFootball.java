package pl.axionis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 */
@SpringBootApplication
public class FacebookFootball {
    public static void main(String[] args) {
        SpringApplication.run(FacebookFootball.class, args);
    }
}
